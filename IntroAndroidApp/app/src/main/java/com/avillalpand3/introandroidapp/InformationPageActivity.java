package com.avillalpand3.introandroidapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.material.switchmaterial.SwitchMaterial;

public class InformationPageActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnSave;
    Spinner spnSeason;
    SeekBar skbTemp;
    SwitchMaterial swchAllergy;
    TextView lblSeekValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        spnSeason = findViewById(R.id.spnSeason);
        skbTemp = findViewById(R.id.skbSeason);
        swchAllergy = findViewById(R.id.swchAllergy);
        lblSeekValue = findViewById(R.id.lblSeekValue);
        btnSave = findViewById(R.id.btnSave);

        btnSave.setOnClickListener(this);
        skbTemp.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String display = String.valueOf(progress);
                lblSeekValue.setText(display);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v != btnSave){
            return;
        }

        String season, allergies;
        int temperature;
        season = spnSeason.getSelectedItem().toString();
        temperature = skbTemp.getProgress();
        allergies = (String) (swchAllergy.isChecked() ? swchAllergy.getTextOn() : swchAllergy.getTextOff());

        Intent intent = new Intent(InformationPageActivity.this, InformationResultsActivity.class);
        intent.putExtra("season", season);
        intent.putExtra("allergies", allergies);
        intent.putExtra("temperature", temperature);
        finish();
        this.startActivity(intent);
    }
}