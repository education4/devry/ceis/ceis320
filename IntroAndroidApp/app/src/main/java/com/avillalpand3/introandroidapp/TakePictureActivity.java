package com.avillalpand3.introandroidapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class TakePictureActivity extends AppCompatActivity {

    private  static int REQUEST_TAKE_PIC = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_picture);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Button btnTakePic = findViewById(R.id.btnTakePic);
        btnTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePic();
            }
        });
    }

    private void takePic(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (null != getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)){
            startActivityForResult(intent, REQUEST_TAKE_PIC);
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent returnIntent){
        if(requestCode == REQUEST_TAKE_PIC && resultCode == RESULT_OK && returnIntent != null) {
            Bitmap bitmap = (Bitmap) returnIntent.getExtras().get("data");
            ImageView imageView = findViewById(R.id.imgFavoriteItem);
            imageView.setImageBitmap(bitmap);
        }
        super.onActivityResult(requestCode, resultCode, returnIntent);
    }
}