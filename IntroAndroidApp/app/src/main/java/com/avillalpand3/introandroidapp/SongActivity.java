package com.avillalpand3.introandroidapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class SongActivity extends AppCompatActivity {

    private VideoView videoView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        videoView = findViewById(R.id.videoView);
        videoView.setMediaController(new MediaController(this));
        Uri video = Uri.parse("android.resource://" + getPackageName()+ "/" +R.raw.civilization_v_gods_and_kings);
        videoView.setVideoURI(video);
        videoView.setZOrderOnTop(true);
    }

    protected void onResume(){
        super.onResume();
        videoView.start();
    }

    protected void onPause(){
        videoView.stopPlayback();
        super.onPause();
    }
}