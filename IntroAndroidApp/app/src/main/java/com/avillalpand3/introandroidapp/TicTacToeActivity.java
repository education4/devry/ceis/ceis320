package com.avillalpand3.introandroidapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.helper.widget.Flow;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Iterator;

public class TicTacToeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button gameGrid[][] = new Button[3][3];
    private Button newGameButton;
    private TextView messageTextView;

    private int turn;
    private boolean gameOver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        gameGrid[0][0] = findViewById(R.id.board_0_0);
        gameGrid[0][1] = findViewById(R.id.board_0_1);
        gameGrid[0][2] = findViewById(R.id.board_0_2);
        gameGrid[1][0] = findViewById(R.id.board_1_0);
        gameGrid[1][1] = findViewById(R.id.board_1_1);
        gameGrid[1][2] = findViewById(R.id.board_1_2);
        gameGrid[2][0] = findViewById(R.id.board_2_0);
        gameGrid[2][1] = findViewById(R.id.board_2_1);
        gameGrid[2][2] = findViewById(R.id.board_2_2);
        newGameButton = findViewById(R.id.button_new_game);
        messageTextView = findViewById(R.id.label_turn);

        newGameButton.setOnClickListener(this);
        for(Button[] row : gameGrid){
            for(Button cell : row){
                cell.setOnClickListener(this);
            }
        }

        resetGame();


    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.button_new_game:
                resetGame();
                break;

            default:
                if (gameOver){
                    messageTextView.setText("That square is taken. Try again.");
                    break;
                }

                Button cell = (Button) v;

                if(cell.getText() != " "){
                   break;
                }

                if (turn % 2 == 0){
                    cell.setText("X");
                    messageTextView.setText(R.string.turn_player_2);
                }else{
                    cell.setText("O");
                    messageTextView.setText(R.string.turn_player_1);
                }

                turn++;
                checkForGameOver();

        }
    }

    public void resetGame(){
        turn = 0;
        gameOver = false;
        messageTextView.setText(R.string.turn_player_1);
        clearGameGrid();
    }

    public void clearGameGrid(){
        for(Button[] row : gameGrid){
            for(Button cell : row){
                cell.setText(" ");
            }
        }
    }

    private void checkForGameOver(){
        //check rows
        for (Button[] row : gameGrid){
            String rowState = "";
            for(Button cell : row){
                rowState += cell.getText();
            }

            if (rowState.equals("XXX") || rowState.equals("OOO")){
                messageTextView.setText(getString(R.string.message_win, rowState.charAt(0)));
                gameOver = true;
                return;
            }
        }

        //check columns
        for(int column = 0; column < 3; column++){
            String columnState = "";
            Button cell;
            for (Button[] row : gameGrid){
                cell = row[column];
                columnState += cell.getText();
            }

            if(columnState.equals("XXX") || columnState.equals("OOO")){
                messageTextView.setText(getString(R.string.message_win, columnState.charAt(0)));
                gameOver = true;
                return;
            }
        }

        String diagonalState = "";
        //check diagonal 0,0 to 2,2
        for(int diagonalPosition = 0; diagonalPosition < 3; diagonalPosition++){
            diagonalState += gameGrid[diagonalPosition][diagonalPosition].getText();
        }
        if(diagonalState.equals("XXX") || diagonalState.equals("OOO")){
            messageTextView.setText((getString(R.string.message_win, diagonalState.charAt(0))));
            gameOver = true;
            return;
        }

        //check diagonal 0,2 to 2,0
        diagonalState =
                (String) gameGrid[0][2].getText() +
                gameGrid[1][1].getText() +
                gameGrid[2][0].getText();
        if(diagonalState.equals("XXX") || diagonalState.equals("OOO")){
            messageTextView.setText((getString(R.string.message_win, diagonalState.charAt(0))));
            gameOver = true;
            return;
        }

        if (turn > 8) {
            messageTextView.setText(R.string.message_tie);
        }
    }
}