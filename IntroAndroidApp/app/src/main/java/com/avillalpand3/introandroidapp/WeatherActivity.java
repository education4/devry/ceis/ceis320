package com.avillalpand3.introandroidapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class WeatherActivity extends AppCompatActivity implements OnCheckedChangeListener,
        OnClickListener {

    // Class Variables
    private RadioButton btnFah;
    private RadioButton btnCel;
    private RadioButton btnKel;
    private RadioGroup tempGroup;
    private EditText txtInputTemp;
    private TextView lblOutputDegF;
    private TextView lblOutputDegC;
    private TextView lblOutputDegK;
    private Button btnConvert;
    // end variable declarations

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        RadioGroup tempGroup = findViewById(R.id.tempGroup);
        tempGroup.setOnCheckedChangeListener(this);

        btnConvert = findViewById(R.id.btnConvert);
        btnConvert.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.mnuExit:
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        // resource gathering
        tempGroup = findViewById(R.id.tempGroup);

        int checkedId = tempGroup.getCheckedRadioButtonId();

        txtInputTemp = ((TextInputLayout) findViewById(R.id.txtInputTemp)).getEditText();
        lblOutputDegF = ((TextInputLayout) findViewById(R.id.lblOutputDegF)).getEditText();
        lblOutputDegC = ((TextInputLayout) findViewById(R.id.lblOutputDegC)).getEditText();
        lblOutputDegK = ((TextInputLayout) findViewById(R.id.lblOutputDegK)).getEditText();
        // end resource gathering

        double temp = Double.parseDouble(String.valueOf(txtInputTemp.getText()));
        double tempC = 0; //used for weatherComment toast

        switch (checkedId) {

            case R.id.btnDegF: {
                double tempF = temp;
                tempC = Math.round((temp - 32) * 5 / 9 * 100.0) / 100.0;
                double tempK = Math.round((temp + 459.67) * 5 / 9 * 100.0) / 100.0;

                lblOutputDegF.setText(tempF + "");
                lblOutputDegC.setText(tempC + "");
                lblOutputDegK.setText(tempK + "");

                break;
            }

            case R.id.btnDegC: {
                double tempF = Math.round(((temp * 9) / 5 + 32) * 100.0) / 100.0;
                tempC = temp;
                double tempK = Math.round((temp + 273.15) * 100.0) / 100.0;

                lblOutputDegF.setText(tempF + "");
                lblOutputDegC.setText(tempC + "");
                lblOutputDegK.setText(tempK + "");

                break;
            }

            case R.id.btnDegK: {
                double tempF = Math.round((temp * 9 / 5 - 459.67) * 100.0) / 100.0;
                tempC = Math.round((temp - 273.15) * 100.0) / 100.0;
                double tempK = temp;

                lblOutputDegF.setText(tempF + "");
                lblOutputDegC.setText(tempC + "");
                lblOutputDegK.setText(tempK + "");

                break;
            }
        }

        displayToast(tempC);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        TextView lblOutput = (TextView) findViewById(R.id.lblOutput);
        switch (checkedId) {
            case R.id.btnDegF:
                lblOutput.setText(R.string.fahConfirmation);
                break;

            case R.id.btnDegC:
                lblOutput.setText(R.string.celConfirmation);
                break;

            case R.id.btnDegK:
                lblOutput.setText(R.string.kelConfirmation);
                break;
        }
    }

    private void displayToast(double tempC) {
        TextView toastGravity = (TextView) findViewById(R.id.toastGravity);
        Toast weatherComment = Toast.makeText(this, "", Toast.LENGTH_LONG);
        weatherComment.setGravity(Gravity.TOP | Gravity.START,
                (int) toastGravity.getX(),
                (int) toastGravity.getY());

        if (tempC > 50) {
            weatherComment.setText("Wow it's hot outside!");
        } else if (tempC > 20) {
            weatherComment.setText("Nice weather we are having");
        } else {
            weatherComment.setText("Brrrrr - it's cold out!");
        }

        weatherComment.show();
    }
}
